import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import styles from "./Footer.module.css";
import FabButton from "../Fab/Fab";

const Footer = () => {
  const location = useLocation();
  const [user, setUser] = useState(
    JSON.parse(localStorage.getItem("profile") as string)
  );

  useEffect(() => {
    setUser(JSON.parse(localStorage.getItem("profile") as string));
  }, [location]);

  return (
    <footer>
      <div className={styles.footerText}></div>
      {user && <FabButton />}
    </footer>
  );
};

export default Footer;
