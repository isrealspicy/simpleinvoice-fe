import { SetStateAction } from "react";

import { History } from "history";
import * as api from "../api/index";
import { AUTH, CREATE_PROFILE } from "./constants";

export const signin =
  (
    formData: {
      firstName: string;
      lastName: string;
      email: string;
      password: string;
      confirmPassword: string;
      profilePicture: string;
      bio: string;
    },
    openSnackbar: (arg0: string) => void,
    setLoading: {
      (value: SetStateAction<boolean>): void;
      (arg0: boolean): void;
    }
  ) =>
  async (dispatch: (arg0: { type: string; data: any }) => void) => {
    try {
      //login the user
      const { data } = await api.signIn(formData);

      console.log(data);
      dispatch({ type: AUTH, data });
      // setLoading(false)
      openSnackbar("Signin successfull");
      // history.push('/dashboard')
      window.location.href = "/dashboard";
    } catch (error: any) {
      // console.log(error?.response?.data?.message);
      openSnackbar(error?.response?.data?.message);
      setLoading(false);
    }
  };

export const signup =
  (
    formData: {
      firstName: string;
      lastName: string;
      email: string;
      password: string;
      confirmPassword: string;
      profilePicture: string;
      bio: string;
    },
    openSnackbar: (arg0: string) => void,
    setLoading: {
      (value: SetStateAction<boolean>): void;
      (arg0: boolean): void;
    }
  ) =>
  async (
    dispatch: (arg0: { type: string; data?: any; payload?: any }) => void
  ) => {
    try {
      //Sign up the user
      const { data } = await api.signUp(formData);
      dispatch({ type: AUTH, data });
      // @ts-ignore
      const { info } = await api.createProfile({
        name: data?.result?.name,
        email: data?.result?.email,
        userId: data?.result?._id,
        phoneNumber: "",
        businessName: "",
        contactAddress: "",
        logo: "",
        website: "",
      });
      dispatch({ type: CREATE_PROFILE, payload: info });
      window.location.href = "/dashboard";
      // history.push('/dashboard')
      openSnackbar("Sign up successfull");
    } catch (error: any) {
      console.log(error);
      openSnackbar(error?.response?.data?.message);
      setLoading(false);
    }
  };

export const forgot =
  (formData: { email: string }) => async (dispatch: any) => {
    try {
      await api.forgot(formData);
    } catch (error) {
      console.log(error);
    }
  };

export const reset =
  (
    formData: {
      password: string;
      token: any; //Sign up the user
    },
    history: string[] | History<unknown>
  ) =>
  async (dispatch: any) => {
    try {
      await api.reset(formData);
      history.push("/dashboard");
    } catch (error) {
      alert(error);
    }
  };
