import { History } from "history";
import * as api from "../api/index";

import {
  ADD_NEW,
  UPDATE,
  DELETE,
  GET_INVOICE,
  FETCH_INVOICE_BY_USER,
  START_LOADING,
  END_LOADING,
} from "./constants";

export const getInvoicesByUser =
  (searchQuery: { search: any }) =>
  async (dispatch: (arg0: { type: string; payload?: any }) => void) => {
    try {
      dispatch({ type: START_LOADING });
      const {
        data: { data },
      } = await api.fetchInvoicesByUser(searchQuery);
      dispatch({ type: FETCH_INVOICE_BY_USER, payload: data });
      dispatch({ type: END_LOADING });
    } catch (error: any) {
      console.log(error.response);
    }
  };

export const getInvoice =
  (id: any) =>
  async (dispatch: (arg0: { type: string; payload: any }) => void) => {
    const user = JSON.parse(localStorage.getItem("profile") as string);

    try {
      const { data } = await api.fetchInvoice(id);
      const businessDetails = await api.fetchProfilesByUser({
        search: user?.result?._id || user?.result?.googleId,
      });
      const invoiceData = { ...data, businessDetails };
      // console.log(invoiceData)
      dispatch({ type: GET_INVOICE, payload: invoiceData });
    } catch (error: any) {
      console.log(error.response);
    }
  };

export const createInvoice =
  (
    invoice: {
      subTotal: number;
      total: number;
      vat: number;
      rates: number;
      currency: any;
      dueDate: number;
      invoiceNumber: string;
      client: any;
      type: string;
      status: string;
      paymentRecords: never[];
      creator: any[];
      items: {
        itemName: string;
        unitPrice: string;
        quantity: string;
        discount: string;
      }[];
      notes: any;
    },
    history: string[] | History<unknown>
  ) =>
  async (dispatch: (arg0: { type: string; payload?: any }) => void) => {
    try {
      dispatch({ type: START_LOADING });
      const { data } = await api.addInvoice(invoice);
      dispatch({ type: ADD_NEW, payload: data });
      history.push(`/invoice/${data._id}`);
      dispatch({ type: END_LOADING });
    } catch (error) {
      console.log(error);
    }
  };

export const updateInvoice =
  (
    id: any,
    invoice: {
      subTotal?: number;
      total?: number;
      vat?: number;
      rates?: number;
      currency?: any;
      dueDate?: number;
      client?: any;
      type?: string;
      status?: string;
      items?: {
        itemName: string;
        unitPrice: string;
        quantity: string;
        discount: string;
      }[];
      notes?: any;
      invoiceNumber?: number;
      creator?: string;
    }
  ) =>
  async (dispatch: (arg0: { type: string; payload: any }) => void) => {
    try {
      const { data } = await api.updateInvoice(id, invoice);
      dispatch({ type: UPDATE, payload: data });
    } catch (error) {
      console.log(error);
    }
  };

export const deleteInvoice =
  (id: any, openSnackbar: (arg0: string) => void) =>
  async (dispatch: (arg0: { type: string; payload: any }) => void) => {
    try {
      await api.deleteInvoice(id);

      dispatch({ type: DELETE, payload: id });
      openSnackbar("Invoice deleted successfully");
    } catch (error: any) {
      console.log(error.response);
    }
  };
