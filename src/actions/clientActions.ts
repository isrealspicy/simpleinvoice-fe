import * as api from "../api/index";

import {
  ADD_NEW_CLIENT,
  UPDATE_CLIENT,
  DELETE_CLIENT,
  FETCH_CLIENTS_BY_USER,
  FETCH_CLIENT,
  START_LOADING,
  END_LOADING,
} from "./constants";

export const getClient =
  (id: any) =>
  async (
    dispatch: (arg0: { type: string; payload?: { client: any } }) => void
  ) => {
    try {
      dispatch({ type: START_LOADING });
      const { data } = await api.fetchClient(id);
      dispatch({ type: FETCH_CLIENT, payload: { client: data } });
    } catch (error) {
      console.log(error);
    }
  };

export const getClientsByUser =
  (searchQuery: { search: any }) =>
  async (dispatch: (arg0: { type: string; payload?: any }) => void) => {
    try {
      dispatch({ type: START_LOADING });
      const {
        data: { data },
      } = await api.fetchClientsByUser(searchQuery);

      dispatch({ type: FETCH_CLIENTS_BY_USER, payload: data });
      dispatch({ type: END_LOADING });
    } catch (error: any) {
      console.log(error.response);
    }
  };

export const createClient =
  (
    client: {
      name: string;
      email: string;
      phone: string;
      address: string;
      userId: any[];
    },
    openSnackbar: (arg0: string) => void
  ) =>
  async (dispatch: (arg0: { type: string; payload: any }) => void) => {
    try {
      const { data } = await api.addClient(client);
      dispatch({ type: ADD_NEW_CLIENT, payload: data });
      openSnackbar("Customer added successfully");
    } catch (error) {
      console.log(error);
    }
  };

export const updateClient =
  (
    id: string,
    client: {
      name: string;
      email: string;
      phone: string;
      address: string;
      userId: any[];
    },
    openSnackbar: (arg0: string) => void
  ) =>
  async (dispatch: (arg0: { type: string; payload: any }) => void) => {
    const { data } = await api.updateClient(id, client);
    dispatch({ type: UPDATE_CLIENT, payload: data });
    openSnackbar("Customer updated successfully");
    try {
    } catch (error) {
      console.log(error);
    }
  };

export const deleteClient =
  (id: any, openSnackbar: (arg0: string) => void) =>
  async (dispatch: (arg0: { type: string; payload: any }) => void) => {
    try {
      await api.deleteClient(id);

      dispatch({ type: DELETE_CLIENT, payload: id });
      openSnackbar("Customer deleted successfully");
    } catch (error) {
      console.log(error);
    }
  };
