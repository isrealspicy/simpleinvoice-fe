import React from "react";
import { TextField, Grid } from "@material-ui/core";
type Props = {
  name: any;
  handleChange: any;
  label: any;
  half: any;
  autoFocus: any;
  type: any;
  value: any;
  multiline: any;
  rows: any;
};
const Input = ({
  name,
  handleChange,
  label,
  half,
  autoFocus,
  type,
  value,
  multiline,
  rows,
}: Partial<Props>) => (
  <Grid item xs={12} sm={half ? 6 : 12}>
    <TextField
      value={value}
      name={name}
      onChange={handleChange}
      variant="outlined"
      required
      fullWidth
      label={label}
      autoFocus={autoFocus}
      type={type}
      multiline={multiline}
      rows={rows}
    />
  </Grid>
);

export default Input;
