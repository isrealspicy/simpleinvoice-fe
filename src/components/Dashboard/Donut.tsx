import { type } from "os";
import React from "react";
import ReactApexChart from "react-apexcharts";

type Props = {
  unpaid: any;
  paid: any;
  partial: any;
};
const Donut = ({ unpaid, paid, partial }: Props) => {
  const series = [unpaid.length, paid.length, partial.length];
  const options = {
    chart: {
      type: "donut",
    },
    labels: ["Unpaid Invoices", "Paid Invoices", "Partially Paid"],
    responsive: [
      {
        breakpoint: 480,
        options: {
          chart: {
            width: 200,
          },
          legend: {
            position: "bottom",
          },
        },
      },
    ],
  };

  return (
    <div>
      <ReactApexChart
        // @ts-ignore
        options={options}
        series={series}
        type="donut"
        width={450}
      />
    </div>
  );
};

export default Donut;
