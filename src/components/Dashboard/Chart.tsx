type Props = {
  paymentHistory: any;
};
function Chart({ paymentHistory }: Props) {
  let paymentDates: string[] = [];
  for (let i = 0; i < paymentHistory.length; i++) {
    const newDate = new Date(paymentHistory[i].datePaid);
    let localDate = newDate.toLocaleDateString();
    paymentDates = [...paymentDates, localDate];
  }

  let paymentReceived: string[] = [];
  for (let i = 0; i < paymentHistory.length; i++) {
    paymentReceived = [...paymentReceived, paymentHistory[i].amountPaid];
  }

  return (
    <div
      style={{
        backgroundColor: "white",
        textAlign: "center",
        width: "90%",
        margin: "10px auto",
        padding: "10px",
      }}
    >
      <br />
    </div>
  );
}

export default Chart;
